﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace MA_Email.Controllers
{
    [Route("api/[controller]/[action]")]
    public class EmailController : Controller
    {
        [HttpPost]
        [ActionName("send-email")]
        public ActionResult SendEmail([FromBody] LohnDto lohn)
        {
            var emailSendung = "Lohnabrechnung von " + lohn.Name + " für eine Auzahlung von "
                + lohn.Betrag + " wurde an die email: " + lohn.Email + " gesendet";
            Console.WriteLine(emailSendung);

            return Ok(emailSendung);
        }
    }
}
